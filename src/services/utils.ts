import { ICharacter, IComic, IStory } from '../ts/interfaces';
import { resultsPerPage } from './constants';
export const debounce = (cb: Function, delay: number) => {
  let timeOut: NodeJS.Timeout;
  return function (...arg: any[]) {
    if (timeOut) {
      clearTimeout(timeOut);
    }
    timeOut = setTimeout(() => {
      cb(...arg);
    }, delay);
  };
};
export const calculateOffset = (currentPage: number) => {
  if (currentPage === 1) {
    return 0;
  } else {
    return (currentPage - 1) * resultsPerPage;
  }
};

export const isCharacter = (
  item: ICharacter | IComic | IStory
): item is ICharacter => {
  return (item as ICharacter).name !== undefined;
};
export const isComic = (item: ICharacter | IComic | IStory): item is IComic => {
  return (item as IComic).stories !== undefined;
};
