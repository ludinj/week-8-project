import { useCallback, useEffect, useState } from 'react';
import { useFetch } from '../../hooks/useFetch';
import {
  hideComicAction,
  setComicsAction,
  showAllComicsAction,
} from '../../redux/reducers/action-creators';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { RootState } from '../../redux/reducers/store';
import { calculateOffset, debounce } from '../../services/utils';

import Pagination from '../../components/pagination/Pagination';
import { comics_base_url, maxResults } from '../../services/constants';
import { useSearchParams } from 'react-router-dom';
import { ESearchParams } from '../../ts/enums';
import './comics.scss';
import ComicCard from '../../components/comic-card/comic-card';

const Comics = () => {
  const comics = useSelector((state: RootState) => state.comics);

  if (comics.length > 0) console.log(comics[0].title);
  const { loading, fetchData } = useFetch();
  const [totalResults, setTotalResults] = useState<number>(0);
  const [searchParams, setSearchParams] = useSearchParams();
  const startsWithQueryValue = searchParams.get(ESearchParams.TITLE);
  const formatQueryValue = searchParams.get(ESearchParams.FORMAT);
  const currentPage = searchParams.get(ESearchParams.PAGE);
  const offset = calculateOffset(currentPage ? parseInt(currentPage) : 1);

  const dispatch = useDispatch();

  const hideComic = bindActionCreators(hideComicAction, dispatch);
  const setComics = useCallback(bindActionCreators(setComicsAction, dispatch), [
    dispatch,
  ]);
  const handleShowComics = bindActionCreators(showAllComicsAction, dispatch);

  useEffect(() => {
    const getComics = async () => {
      setComics([]);
      const formatQuery = formatQueryValue ? `&format=${formatQueryValue}` : '';
      const characterNameQuery = startsWithQueryValue
        ? `&titleStartsWith=${startsWithQueryValue}`
        : '';
      try {
        const response = await fetchData(
          `${comics_base_url}${formatQuery}${characterNameQuery}&offset=${offset}`
        );

        //set total results to max 1000
        if (response.total > maxResults) {
          setTotalResults(maxResults);
        } else {
          setTotalResults(response.total);
        }

        setComics(response.results);
      } catch (error) {
        console.log(error);
      }
    };

    getComics();
  }, [setComics, startsWithQueryValue, formatQueryValue, offset, fetchData]);

  const changeFormat = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value) {
      searchParams.set(ESearchParams.FORMAT, e.target.value);
      searchParams.delete(ESearchParams.PAGE);
    } else {
      searchParams.delete(ESearchParams.FORMAT);
      searchParams.delete(ESearchParams.PAGE);
    }
    setSearchParams(searchParams);
  };

  const handleSearchByTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value) {
      searchParams.set(ESearchParams.TITLE, e.target.value);
      searchParams.delete(ESearchParams.PAGE);
    } else {
      searchParams.delete(ESearchParams.TITLE);
      searchParams.delete(ESearchParams.PAGE);
    }
    setSearchParams(searchParams);
  };
  const debounceSearch = debounce(handleSearchByTitle, 1000); // 1sec delay

  return (
    <div className='comics__container'>
      <div className='search-input'>
        <input
          type='text'
          name='characterName'
          placeholder='Search a comic...'
          onChange={debounceSearch}
        />
      </div>
      <div className='filters'>
        <h2>Filter by:</h2>
        <label htmlFor='format'>Format.</label>
        <select name='format' onChange={(e) => changeFormat(e)}>
          <option value=''>All</option>
          <option value={ESearchParams.COMIC}>{ESearchParams.COMIC}</option>
          <option value={ESearchParams.MAGAZINE}>
            {ESearchParams.MAGAZINE}
          </option>
          <option value={ESearchParams.TRADE_PAPERBACK}>
            {ESearchParams.TRADE_PAPERBACK}
          </option>
          <option value={ESearchParams.HARDCOVER}>
            {ESearchParams.HARDCOVER}
          </option>
          <option value={ESearchParams.DIGEST}>{ESearchParams.DIGEST}</option>
          <option value={ESearchParams.GRAPHIC_NOVEL}>
            {ESearchParams.GRAPHIC_NOVEL}
          </option>
          <option value={ESearchParams.DIGITAL_COMIC}>
            {ESearchParams.DIGITAL_COMIC}
          </option>
          <option value={ESearchParams.INFINITE_COMIC}>
            {ESearchParams.INFINITE_COMIC}
          </option>
        </select>

        <button className='control__button' onClick={handleShowComics}>
          Show All
        </button>
      </div>
      {!loading ? (
        <div className='grid'>
          {comics.length > 0 ? (
            <>
              {comics.map((comic) =>
                !comic.hidden ? (
                  <div className='card' key={comic.id}>
                    <button
                      className='control__button'
                      onClick={() => hideComic(comic.id)}
                    >
                      Hide
                    </button>
                    <ComicCard comic={comic} />
                  </div>
                ) : null
              )}
            </>
          ) : (
            <h3>No results.</h3>
          )}
        </div>
      ) : (
        <h2>Loading..</h2>
      )}
      <Pagination
        totalResults={totalResults}
        currentPage={currentPage ? parseInt(currentPage) : 1}
        searchParams={searchParams}
        setSearchParams={setSearchParams}
      />
    </div>
  );
};

export default Comics;
