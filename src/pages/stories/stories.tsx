import React from 'react';

import { useCallback, useEffect, useState } from 'react';
import { ICharacter } from '../../ts/interfaces';
import { useFetch } from '../../hooks/useFetch';
import {
  hideStoryAction,
  setStoriesAction,
  showAllStoriesAction,
} from '../../redux/reducers/action-creators';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { RootState } from '../../redux/reducers/store';
import { calculateOffset } from '../../services/utils';
import Pagination from '../../components/pagination/Pagination';
import {
  maxResults,
  REACT_APP_API_PUBLIC_KEY3,
  REACT_APP_BASE_API_URL,
  stories_base_url,
} from '../../services/constants';
import { useSearchParams } from 'react-router-dom';
import { ESearchParams } from '../../ts/enums';
import './stories.scss';
import StoryCard from '../../components/story-card/story-card';

const Stories = () => {
  const { loading, fetchData } = useFetch();
  const [totalResults, setTotalResults] = useState<number>(0);
  const [characters, setCharacters] = useState<ICharacter[]>([]);
  const [searchParams, setSearchParams] = useSearchParams();
  const characterQueryValue = searchParams.get(ESearchParams.CHARACTER);
  const currentPage = searchParams.get(ESearchParams.PAGE);
  const offset = calculateOffset(currentPage ? parseInt(currentPage) : 1);
  const stories = useSelector((state: RootState) => state.stories);
  const dispatch = useDispatch();
  const setStories = useCallback(
    bindActionCreators(setStoriesAction, dispatch),
    [dispatch]
  );
  const handleShowStories = bindActionCreators(showAllStoriesAction, dispatch);
  const hideStory = bindActionCreators(hideStoryAction, dispatch);

  useEffect(() => {
    const getComics = async () => {
      setStories([]);
      const charactersQuery = characterQueryValue
        ? `&characters=${characterQueryValue}`
        : '';

      try {
        const response = await fetchData(
          `${stories_base_url}${charactersQuery}&offset=${offset}`
        );

        //set total results to max 1000
        if (response.total > maxResults) {
          setTotalResults(maxResults);
        } else {
          setTotalResults(response.total);
        }

        setStories(response.results);
      } catch (error) {
        console.log(error);
      }
    };

    const getCharacters = async () => {
      try {
        const response = await fetchData(
          `${REACT_APP_BASE_API_URL}/characters?apikey=${REACT_APP_API_PUBLIC_KEY3}&limit=100`
        );
        setCharacters(response.results);
      } catch (error) {
        console.log(error);
      }
    };
    getCharacters();
    getComics();
  }, [setStories, characterQueryValue, offset, fetchData]);

  const changeCharacter = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value) {
      searchParams.set(ESearchParams.CHARACTER, e.target.value);
      searchParams.delete(ESearchParams.PAGE);
    } else {
      searchParams.delete(ESearchParams.CHARACTER);
      searchParams.delete(ESearchParams.PAGE);
    }
    setSearchParams(searchParams);
  };

  return (
    <div className='stories__container'>
      <div className='filters'>
        <h2>Filter by:</h2>
        <label htmlFor='character'>Character.</label>
        <select name='character' onChange={(e) => changeCharacter(e)}>
          <option value=''>All</option>
          {characters.map((character) =>
            character.stories.available > 0 ? (
              <option value={character.id} key={character.id}>
                {character.name}
              </option>
            ) : null
          )}
        </select>

        <button className='control__button' onClick={handleShowStories}>
          Show All
        </button>
      </div>
      {!loading ? (
        <div className='grid'>
          {stories.map((story) =>
            !story.hidden ? (
              <div className='card' key={story.id}>
                <button
                  className='control__button'
                  onClick={() => hideStory(story.id)}
                >
                  Hide
                </button>
                <StoryCard story={story} />
              </div>
            ) : null
          )}
        </div>
      ) : (
        <h2>Loading..</h2>
      )}
      <Pagination
        totalResults={totalResults}
        currentPage={currentPage ? parseInt(currentPage) : 1}
        searchParams={searchParams}
        setSearchParams={setSearchParams}
      />
    </div>
  );
};

export default Stories;
