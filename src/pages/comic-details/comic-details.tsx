import React, { useEffect, useState } from 'react';

import { useParams } from 'react-router-dom';
import { IComic } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';
import './comic-details.scss';

import { useFetch } from '../../hooks/useFetch';
import { bindActionCreators } from 'redux';
import { addBookmarkAction } from '../../redux/reducers/action-creators';
import { useDispatch } from 'react-redux';
import {
  REACT_APP_API_PUBLIC_KEY3,
  REACT_APP_BASE_API_URL,
} from '../../services/constants';
const ComicDetails = () => {
  const { fetchData } = useFetch();
  const { comicId } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [comic, setComic] = useState<IComic | undefined>(undefined);
  const [isDisableButton, SetIsDisableButton] = useState<boolean>(false);

  const bookmarkItem = bindActionCreators(addBookmarkAction, dispatch);
  useEffect(() => {
    const getComic = async () => {
      try {
        const response = await fetchData(
          `${REACT_APP_BASE_API_URL}/comics/${comicId}?apikey=${REACT_APP_API_PUBLIC_KEY3}`
        );
        console.log(response);

        const comic = response.results[0];

        setComic(comic);
      } catch (error) {
        navigate('*');
      }
    };
    getComic();
  }, [comicId, navigate, fetchData]);

  const handleAddToBookmarks = () => {
    if (comic) {
      bookmarkItem(comic);
      SetIsDisableButton(true);
      alert('Item Bookmarked');
    }
  };

  return (
    <div className='comic-details__page'>
      {comic ? (
        <div className='comic-details__container'>
          <section className='image__section'>
            <img
              src={comic.thumbnail.path + '.' + comic.thumbnail.extension}
              alt={comic.title}
            />
            <section className='info__section'>
              <h1>{comic.title}</h1>
              {comic.description ? (
                <p>{comic.description}</p>
              ) : (
                <p>No description available</p>
              )}
              <button
                onClick={handleAddToBookmarks}
                className='bookmark'
                disabled={isDisableButton}
              >
                bookmark
              </button>
            </section>
          </section>

          <div className='comic__appearances'>
            <div>
              <h4>Characters</h4>
              <ul>
                {comic.characters.items.map((item, idx) => (
                  <li key={idx}>{item.name}</li>
                ))}
              </ul>
            </div>
            <div>
              <h4>Stories</h4>
              <ul>
                {comic.stories.items.map((item, idx) => (
                  <li key={idx}>{item.name}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      ) : (
        <h2>Loading...</h2>
      )}
    </div>
  );
};

export default ComicDetails;
