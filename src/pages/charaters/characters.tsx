import { useCallback, useEffect, useState } from 'react';
import { IComic, IStory } from '../../ts/interfaces';
import {
  hideCharacterAction,
  setCharactersAction,
  showAllCharactersAction,
} from '../../redux/reducers/action-creators';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { RootState } from '../../redux/reducers/store';
import { calculateOffset, debounce } from '../../services/utils';

import Pagination from '../../components/pagination/Pagination';
import {
  characters_base_url,
  maxOptionLength,
  maxResults,
  REACT_APP_API_PUBLIC_KEY3,
  REACT_APP_BASE_API_URL,
} from '../../services/constants';
import { useSearchParams } from 'react-router-dom';
import { ESearchParams } from '../../ts/enums';
import { useFetch } from '../../hooks/useFetch';
import './characters.scss';
import HeroCard from '../../components/hero-card/hero-card';

const Characters = () => {
  const characters = useSelector((state: RootState) => state.characters);
  const [comics, setComics] = useState<IComic[]>([]);
  const [stories, setStories] = useState<IStory[]>([]);
  const [totalResults, setTotalResults] = useState<number>(0);
  const [searchParams, setSearchParams] = useSearchParams();
  const startsWithQueryValue = searchParams.get(ESearchParams.NAME);
  const storyQueryValue = searchParams.get(ESearchParams.STORY);
  const comicQueryValue = searchParams.get(ESearchParams.COMIC);
  const currentPage = searchParams.get(ESearchParams.PAGE);
  const offset = calculateOffset(currentPage ? parseInt(currentPage) : 1);
  const dispatch = useDispatch();
  const { loading, fetchData } = useFetch();
  const hideCharacter = bindActionCreators(hideCharacterAction, dispatch);
  const setCharacters = useCallback(
    bindActionCreators(setCharactersAction, dispatch),
    [dispatch]
  );
  const showAllCharacters = bindActionCreators(
    showAllCharactersAction,
    dispatch
  );

  useEffect(() => {
    const getCharacters = async () => {
      setCharacters([]);
      const storyQuery = storyQueryValue ? `&stories=${storyQueryValue}` : '';
      const comicsQuery = comicQueryValue ? `&comics=${comicQueryValue}` : '';
      const characterNameQuery = startsWithQueryValue
        ? `&nameStartsWith=${startsWithQueryValue}`
        : '';
      try {
        const response = await fetchData(
          `${characters_base_url}${storyQuery}${comicsQuery}${characterNameQuery}&offset=${offset}`
        );

        if (response.total > maxResults) {
          setTotalResults(maxResults);
        } else {
          setTotalResults(response.total);
        }
        setCharacters(response.results);
      } catch (err) {
        console.log(err);
      }
    };

    const getComics = async () => {
      try {
        const response = await fetchData(
          `${REACT_APP_BASE_API_URL}/comics?apikey=${REACT_APP_API_PUBLIC_KEY3}&limit=100`
        );
        setComics(response.results);
      } catch (error) {
        console.log(error);
      }
    };
    const getStories = async () => {
      try {
        const response = await fetchData(
          `${REACT_APP_BASE_API_URL}/stories?apikey=${REACT_APP_API_PUBLIC_KEY3}&limit=100`
        );

        setStories(response.results);
      } catch (error) {
        console.log(error);
      }
    };

    getStories();
    getComics();
    getCharacters();
  }, [
    setCharacters,
    startsWithQueryValue,
    comicQueryValue,
    storyQueryValue,
    offset,
    fetchData,
  ]);

  const changeStory = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value) {
      searchParams.set(ESearchParams.STORY, e.target.value);
      searchParams.delete(ESearchParams.PAGE);
      setSearchParams(searchParams);
    } else {
      searchParams.delete(ESearchParams.STORY);
      searchParams.delete(ESearchParams.PAGE);
      setSearchParams(searchParams);
    }
  };
  const changeComics = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value) {
      searchParams.set(ESearchParams.COMIC, e.target.value);
      searchParams.delete(ESearchParams.PAGE);
      setSearchParams(searchParams);
    } else {
      searchParams.delete(ESearchParams.COMIC);
      searchParams.delete(ESearchParams.PAGE);
      setSearchParams(searchParams);
    }
  };

  const handleSearchByName = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value) {
      searchParams.set(ESearchParams.NAME, e.target.value);
      searchParams.delete('page');
      setSearchParams(searchParams);
    } else {
      searchParams.delete(ESearchParams.NAME);
      searchParams.delete(ESearchParams.PAGE);
      setSearchParams(searchParams);
    }
  };
  const debounceSearch = debounce(handleSearchByName, 1000); // 1sec delay

  return (
    <div className='characters__container'>
      <div className='search-input'>
        <input
          type='text'
          name='characterName'
          placeholder='Search a character...'
          onChange={debounceSearch}
        />
      </div>
      <div className='filters'>
        <h2>Filter by:</h2>
        <label htmlFor='comics'>Comics.</label>
        <select name='comics' onChange={(e) => changeComics(e)}>
          <option value=''>All</option>

          {comics?.map((comic, idx) =>
            comic.characters.items.length > 0 ? (
              <option value={comic.id} key={idx}>{`${comic.title.substring(
                0,
                maxOptionLength
              )}...`}</option>
            ) : null
          )}
        </select>
        <label htmlFor='stories'>stories.</label>

        <select name='stories' id='' onChange={(e) => changeStory(e)}>
          <option value=''>All</option>
          {stories?.map((story, idx) => (
            <option value={story.id} key={idx}>{`${story.title.substring(
              0,
              maxOptionLength
            )}...`}</option>
          ))}
        </select>

        <button className='control__button' onClick={showAllCharacters}>
          Show All
        </button>
      </div>

      {!loading ? (
        <div className='grid'>
          {characters.length > 0 ? (
            <>
              {characters.map((character) =>
                !character.hidden ? (
                  <div className='card' key={character.id}>
                    <button
                      className='control__button'
                      onClick={() => hideCharacter(character.id)}
                    >
                      Hide
                    </button>
                    <HeroCard character={character} />
                  </div>
                ) : null
              )}
            </>
          ) : (
            <h3>No results.</h3>
          )}
        </div>
      ) : (
        <h2>Loading..</h2>
      )}
      <Pagination
        totalResults={totalResults}
        currentPage={currentPage ? parseInt(currentPage) : 1}
        searchParams={searchParams}
        setSearchParams={setSearchParams}
      />
    </div>
  );
};

export default Characters;
