import React, { useMemo } from 'react';
import { resultsPerPage } from '../../services/constants';
import { EArrowAction } from '../../ts/enums';
import './pagination.scss';

type PaginationProps = {
  totalResults: number;
  currentPage: number;
  searchParams: URLSearchParams;
  setSearchParams: (params: URLSearchParams) => void;
};

const Pagination = ({
  totalResults,
  currentPage,
  searchParams,
  setSearchParams,
}: PaginationProps) => {
  const pages = useMemo(() => {
    const pages = [];
    for (let i = 1; i <= Math.ceil(totalResults / resultsPerPage); i++) {
      pages.push(i);
    }
    return pages;
  }, [totalResults]);

  const handleArrowClick = (action: string) => {
    window.scroll(0, 0);
    if (action === EArrowAction.LEFT && currentPage > 1) {
      searchParams.set('page', (currentPage - 1).toString());
      setSearchParams(searchParams);
    }
    if (action === EArrowAction.RIGHT && pages.length > currentPage) {
      searchParams.set('page', (currentPage + 1).toString());
      setSearchParams(searchParams);
    }
  };
  const handleButtonClick = (page: number) => {
    window.scroll(0, 0);

    searchParams.set('page', page.toString());
    setSearchParams(searchParams);
  };
  return (
    <div className='pagination'>
      <div className='change-page__buttons'>
        <button
          className='change-page__button'
          disabled={currentPage <= 1}
          onClick={() => handleArrowClick(EArrowAction.LEFT)}
        >
          Previous
        </button>
        <button
          className='change-page__button'
          disabled={currentPage >= pages.length}
          onClick={() => handleArrowClick(EArrowAction.RIGHT)}
        >
          Next
        </button>
      </div>

      <div className='pages'>
        {pages.map((page, index) => {
          return (
            <button
              key={index}
              onClick={() => handleButtonClick(page)}
              className={`page__button ${page === currentPage ? 'active' : ''}`}
            >
              {page}
            </button>
          );
        })}
      </div>
    </div>
  );
};

export default Pagination;
