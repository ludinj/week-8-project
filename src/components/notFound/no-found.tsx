import './no-found.scss';
const PageNotFound = () => {
  return (
    <div className='not__found'>
      <h1>404 resourse not found</h1>
    </div>
  );
};

export default PageNotFound;
